<?php

namespace App\DataFixtures;
 
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
 
class UserFixtures extends Fixture
{
    private $passwordEncoder;
 
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }
 
    public function load(ObjectManager $manager)
    {
        // configuring in which language we want to create these fixtures
        $faker = Faker\Factory::create('fr_FR');
 
        // boucle qui crée 10 users
        for ($i = 0; $i < 10; $i++) {
            $user = new User();
            $user->setName($faker->name);
            $user->setEmail(sprintf('userdemo%d@example.com', $i));
            if ($i<6)
            {
                $user->setRoles(['ROLE_USER','ROLE_ADMIN']);
            }
            else
            {
                $user->setRoles(['ROLE_USER']);
            }
            
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'userdemo'
            ));
            $manager->persist($user);
        }
 
        $manager->flush();
 
    }
}