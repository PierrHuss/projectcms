<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Images
 *
 * @ORM\Table(name="images")
 * @ORM\Entity
 */
class Images
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="path", type="text", length=0, nullable=true)
     */
    private $path;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Blocks", mappedBy="image_id", cascade={"persist", "remove"})
     */
    private $block_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function getBlockId(): ?Blocks
    {
        return $this->block_id;
    }

    public function setBlockId(?Blocks $block_id): self
    {
        $this->block_id = $block_id;

        // set (or unset) the owning side of the relation if necessary
        $newImage_id = $block_id === null ? null : $this;
        if ($newImage_id !== $block_id->getImageId()) {
            $block_id->setImageId($newImage_id);
        }

        return $this;
    }


}
