<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Pages;

/**
 * Blocks
 *
 * @ORM\Table(name="blocks")
 * @ORM\Entity
 */
class Blocks
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", length=0, nullable=false)
     */
    private $content;

/**
 *  @ORM\Column(name="block_title", type="string", length=255, nullable=false) 
 */
   private $block_title;

   /**
    * @ORM\OneToOne(targetEntity="App\Entity\Images", inversedBy="block_id", cascade={"persist", "remove"})
    */
   private $image_id;

   /**
    * @ORM\ManyToOne(targetEntity="App\Entity\Pages", inversedBy="block_id")
    * @ORM\JoinColumn(nullable=false)
    */
   private $page_number;

/**
 *  @ORM\Column(name="order", type="integer", nullable=false) 
 */
private $order;

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->order;
    }

    public function setOrder(int $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getBlockTitle(): ?string
    {
        return $this->block_title;
    }

    public function setBlockTitle(string $block_title): self
    {
        $this->block_title = $block_title;

        return $this;
    }

    public function getImageId(): ?Images
    {
        return $this->image_id;
    }

    public function setImageId(?Images $image_id): self
    {
        $this->image_id = $image_id;

        return $this;
    }

    public function getPageNumber(): ?Pages
    {
        return $this->page_number;
    }

    public function setPageNumber(?Pages $page_number): self
    {
        $this->page_number = $page_number;

        return $this;
    }

}
