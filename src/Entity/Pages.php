<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;


/**
 * Pages
 *
 * @ORM\Table(name="pages")
 * @ORM\Entity
 */
class Pages
{   
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime", nullable=false)
     */
    private $creationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_time", type="datetime", nullable=false)
     */
    private $modifiedTime;

    /**
     * @var string
     *
     * @ORM\Column(name="category", type="string", length=255, nullable=false)
     */
    private $category;

    /**
     * @var string
     * 
     * @ORM\Column(name="title", type="string", length=50, nullable=false)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Blocks", mappedBy="page_number", orphanRemoval=true)
     */
    private $block_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreationDate(): ?\DateTimeInterface
    {
        return $this->creationDate;
    }

    public function setCreationDate(\DateTimeInterface $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }

    public function getModifiedTime(): ?\DateTimeInterface
    {
        return $this->modifiedTime;
    }

    public function setModifiedTime(\DateTimeInterface $modifiedTime): self
    {
        $this->modifiedTime = $modifiedTime;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
 
    public function __construct() {
        $this->block_id = new ArrayCollection();
    }

    /**
     * @return Collection|Blocks[]
     */
    public function getBlockId(): Collection
    {
        return $this->block_id;
    }

    public function addBlockId(Blocks $blockId): self
    {
        if (!$this->block_id->contains($blockId)) {
            $this->block_id[] = $blockId;
            $blockId->setPageNumber($this);
        }

        return $this;
    }

    public function removeBlockId(Blocks $blockId): self
    {
        if ($this->block_id->contains($blockId)) {
            $this->block_id->removeElement($blockId);
            // set the owning side to null (unless already changed)
            if ($blockId->getPageNumber() === $this) {
                $blockId->setPageNumber(null);
            }
        }

        return $this;
    }
}

