<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Images;
use App\Entity\Blocks;
use App\Form\Type\NewImageType;

class NewBlockType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('block_title', TextType::class)
            ->add('content', TextType::class)
            ->add('image', NewImageType::class)
            ->add('order', HiddenType::class)
        ;
    }
}