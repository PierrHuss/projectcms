<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use App\Entity\Pages;
use App\Entity\Blocks;

class NewPageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class)
            ->add('category', TextType::class)
            ->add('block', CollectionType::class, [
                'entry_type' => NewBlockType::class,
                'entry_options' => ['label' => false],
                'allow_add' => true
            ])
            ->add('save', SubmitType::class)
        ;
    }
}