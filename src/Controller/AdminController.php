<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\Type\NewPageType;
use App\Form\Type\NewBlockType;


class AdminController extends AbstractController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index()
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

     /**
     * @Route("/admin/newpost", name="newpost")
     */

    public function newPost(Request $request)
    {
        $newPage = new Pages();
        $newPage->setCreationDate(now());
        $newPage->setModifiedTime(now());
        $formpage = $this->createForm(NewPageType::class, $Pages);
       
        return $this->render('admin/new.html.twig',[
            'formpage'=>$formpage
        ]);
    }

    /**
     * @Route("/admin/editpost/{id}", name="editpost")
     */

    public function editPost()
    {
        return $this->render('admin/edit.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/admin/deletepost/{id}", name="deletepost")
     */

    public function deletePost()
    {
        return $this->render('admin/delete.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }
}
